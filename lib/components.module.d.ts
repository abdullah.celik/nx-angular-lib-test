import * as i0 from "@angular/core";
import * as i1 from "./button/button.component";
import * as i2 from "./input/input.component";
import * as i3 from "@angular/common";
export declare class ComponentsModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<ComponentsModule, [typeof i1.ButtonComponent, typeof i2.InputComponent], [typeof i3.CommonModule], [typeof i1.ButtonComponent, typeof i2.InputComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<ComponentsModule>;
}
//# sourceMappingURL=components.module.d.ts.map