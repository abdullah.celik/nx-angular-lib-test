import { ɵɵdefineComponent, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, ɵsetClassMetadata, Component, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

class ButtonComponent {
    constructor() { }
    ngOnInit() {
    }
}
ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
ButtonComponent.ɵcmp = ɵɵdefineComponent({ type: ButtonComponent, selectors: [["nx-test-button"]], decls: 2, vars: 0, template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "p");
        ɵɵtext(1, "button works!");
        ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(ButtonComponent, [{
        type: Component,
        args: [{
                selector: 'nx-test-button',
                templateUrl: './button.component.html',
                styleUrls: ['./button.component.css']
            }]
    }], function () { return []; }, null); })();

class InputComponent {
    constructor() { }
    ngOnInit() {
    }
}
InputComponent.ɵfac = function InputComponent_Factory(t) { return new (t || InputComponent)(); };
InputComponent.ɵcmp = ɵɵdefineComponent({ type: InputComponent, selectors: [["nx-test-input"]], decls: 2, vars: 0, template: function InputComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "p");
        ɵɵtext(1, "input works!");
        ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(InputComponent, [{
        type: Component,
        args: [{
                selector: 'nx-test-input',
                templateUrl: './input.component.html',
                styleUrls: ['./input.component.css']
            }]
    }], function () { return []; }, null); })();

class ComponentsModule {
}
ComponentsModule.ɵmod = ɵɵdefineNgModule({ type: ComponentsModule });
ComponentsModule.ɵinj = ɵɵdefineInjector({ factory: function ComponentsModule_Factory(t) { return new (t || ComponentsModule)(); }, imports: [[CommonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(ComponentsModule, { declarations: [ButtonComponent, InputComponent], imports: [CommonModule], exports: [ButtonComponent, InputComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(ComponentsModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule],
                declarations: [ButtonComponent, InputComponent],
                exports: [ButtonComponent, InputComponent],
            }]
    }], null, null); })();

const arrayItemFinder = (array, item) => {
    const itemIndex = array.indexOf(item);
    if (itemIndex !== -1)
        return array[itemIndex];
    return undefined;
};

const stringTopperCase = (text) => {
    return text.toUpperCase();
};

/**
 * Generated bundle index. Do not edit.
 */

export { ButtonComponent, ComponentsModule, InputComponent, arrayItemFinder, stringTopperCase };
//# sourceMappingURL=nx-test-components.js.map
