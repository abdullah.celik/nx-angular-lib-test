import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import * as i0 from "@angular/core";
export class ComponentsModule {
}
ComponentsModule.ɵmod = i0.ɵɵdefineNgModule({ type: ComponentsModule });
ComponentsModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ComponentsModule_Factory(t) { return new (t || ComponentsModule)(); }, imports: [[CommonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ComponentsModule, { declarations: [ButtonComponent, InputComponent], imports: [CommonModule], exports: [ButtonComponent, InputComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ComponentsModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule],
                declarations: [ButtonComponent, InputComponent],
                exports: [ButtonComponent, InputComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=components.module.js.map