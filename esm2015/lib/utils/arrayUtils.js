export const arrayItemFinder = (array, item) => {
    const itemIndex = array.indexOf(item);
    if (itemIndex !== -1)
        return array[itemIndex];
    return undefined;
};
//# sourceMappingURL=arrayUtils.js.map