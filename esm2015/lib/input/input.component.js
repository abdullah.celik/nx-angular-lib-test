import { Component } from '@angular/core';
import * as i0 from "@angular/core";
export class InputComponent {
    constructor() { }
    ngOnInit() {
    }
}
InputComponent.ɵfac = function InputComponent_Factory(t) { return new (t || InputComponent)(); };
InputComponent.ɵcmp = i0.ɵɵdefineComponent({ type: InputComponent, selectors: [["nx-test-input"]], decls: 2, vars: 0, template: function InputComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "p");
        i0.ɵɵtext(1, "input works!");
        i0.ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(InputComponent, [{
        type: Component,
        args: [{
                selector: 'nx-test-input',
                templateUrl: './input.component.html',
                styleUrls: ['./input.component.css']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=input.component.js.map